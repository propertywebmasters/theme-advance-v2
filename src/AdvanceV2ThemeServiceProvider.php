<?php

namespace PropertyWebmasters\AdvanceV2Theme;

use Illuminate\Support\ServiceProvider;

class AdvanceV2ThemeServiceProvider extends ServiceProvider
{
    private const THEME = 'advance-v2';

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadViewsFrom(__DIR__, self::THEME.'-theme');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/' => base_path('resources/views/vendor/themes/'.self::THEME.'/config'),
            __DIR__ . '/views/' => base_path('resources/views/vendor/themes/'.self::THEME.'/views'),
        ], self::THEME.'-theme-views');
        
        $this->publishes([
            __DIR__ . '/assets/' => public_path('themes/'.self::THEME.'/assets'),
        ], self::THEME.'-theme-assets');
    }
}
