@php
    // if the slide is empty then we will inherit from theme default
    use App\Models\Slide;use App\Models\TenantFeature;use App\Services\Content\ContentBlockService;use App\Support\ThemeManager;if($slides->count() === 0) {
        $imagePath = ThemeManager::backgroundCSS('home.hero', tenantManager()->getTenant(), true);
        $slide = new Slide;
        $slide->image = $imagePath;
        $slide->title = optional(app()->make(ContentBlockService::class)->getByIdentifier('home', 'hero-slogan', tenantManager()->getTenant(), app()->getLocale()))->content;
        $slides = collect()->push($slide);
    } else {
        $slide = $slides->first();
    }

    $slideSubtitlesEnabled = hasFeature(TenantFeature::FEATURE_SLIDE_SUBTITLES);
    $slideSecondaryTitleEnabled = hasFeature(TenantFeature::FEATURE_SLIDE_SECONDARY_TITLE)
@endphp

<div class="min-h-screen">
    <div class="grid grid-cols-5">
        <div class="col-span-2 h-full hidden xl:block" style="background: var(--ap-nav-bg);"></div>
        <div class="col-span-5 xl:col-span-3">
            <div id="custom-home-slides" data-speed="12000" class="min-h-screen w-full">
                @php $i = 1 @endphp
                @foreach ($slides as $slide)
                    @php
                        $displayClass = $i === 1 ? '' : ' hidden';
                        $active = $i === 1 ? 'active' : '';
                        $tag = $i === 1 ? 'h1' : 'h2'
                    @endphp
                    <div class="min-h-screen relative slide slide-{{$i}} {{ $active }} {{ $displayClass }}" data-slide-number="{{ $i }}" data-title="{{ $slide->title }}" @if ($slideSubtitlesEnabled && $slide->subtitle) data-subtitle="{{ $slide->subtitle }}" @endif>
                        <div class="min-h-screen lg:flex lg:justify-between items-center align-items-center lg:flex-row">
                            <div class="w-full min-h-screen">
                                <div class="bg-lazy-load center-cover-bg min-h-screen w-full" data-style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('{{ assetPath($slide->image) }}')">
                                </div>
                            </div>
                        </div>
                    </div>
                @php $i++ @endphp
                @endforeach
            </div>
        </div>
    </div>

    <div class="container mx-auto absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 px-4 md:px-0">
        <div class="w-full text-left">
            <div class="mb-10">
                <h1 id="slide-title" class="text-5xl tracking-tight header-text primary-text mb-4 text-white">{!! $slide->title !!}</h1>

                @if ($slideSecondaryTitleEnabled && $slide->secondary_title)
                    <h1 id="slide-secondary-title" class="text-6xl xl:text-7xl primary-text secondary-header-text" style="line-height: 0.4;">{!! $slide->secondary_title !!}</h1>
                @endif
            </div>

            @if ($slideSubtitlesEnabled && $slide->subtitle)
                <h3 id="slide-subtitle" class="block pb-6 text-lg text-white mb-10 pt-4">{{ $slide->subtitle }}</h3>
            @endif
        </div>

        <div class="flex flex-col sm:flex-row items-start sm:items-center mb-10">
            @if (hasFeature(\App\Models\TenantFeature::FEATURE_VALUATION_SYSTEM))
                <a class="valuation-button whitespace-nowrap lg:text-sm text-base text-center font-bold rounded-3xl max-w-xs block py-3 px-8 transition-all mb-4 sm:mb-0 w-2/3 primary-bg text-white"
                href="{{ localeUrl('/valuation') }}">{{ trans('button.book_a_valuation') }}</a>
            @endif

            <a style="border-color: var(--ap-cta-bg);" class="find-a-property-button whitespace-nowrap lg:text-sm text-base text-center font-bold rounded-3xl max-w-xs block sm:ml-4 py-3 px-8 transition-all w-2/3 border text-white" href="{{ localeUrl('/all-properties-for-sale') }}">
                {{ trans('button.find_a_property') }}
            </a>
        </div>
    </div>
</div>
