@php
    use App\Models\TenantFeature;

    $slideSecondaryTitleEnabled = hasFeature(TenantFeature::FEATURE_SLIDE_SECONDARY_TITLE);
    $slideSubtitlesEnabled = hasFeature(TenantFeature::FEATURE_SLIDE_SUBTITLES);
@endphp

<div class="h-full absolute top-0 left-0 h-full w-full grid grid-cols-4 xl:grid-cols-5">

    <div class="col-span-4 lg:col-span-2 container mx-auto px-4 md:px-0 h-full flex items-center" style="background: var(--ap-nav-bg);">

        <div class="container mx-auto px-4">
            <div class="lg:pl-12 lg:pr-4">
                <div class="w-full text-left">
                    <div class="mb-2">
                        <h1 id="slide-title" class="text-5xl tracking-tight header-text primary-text mb-2 text-white">{!! $video->title !!}</h1>

                        @if ($slideSecondaryTitleEnabled && $video->secondary_title)
                            <h1 id="slide-secondary-title" class="text-6xl xl:text-7xl primary-text secondary-header-text">{!! $video->secondary_title !!}</h1>
                        @endif
                    </div>

                    @if ($slideSubtitlesEnabled && $video->subtitle)
                        <h3 id="slide-subtitle" class="block pb-6 text-lg text-white mb-10 pt-4">{!! $video->subtitle !!}</h3>
                    @endif
                </div>

                <div class="flex flex-col sm:flex-row items-start sm:items-center mb-10">
                    @if (hasFeature(TenantFeature::FEATURE_VALUATION_SYSTEM))
                        <a class="valuation-button whitespace-nowrap lg:text-sm text-base text-center font-bold rounded-3xl max-w-xs block py-3 px-8 transition-all mb-4 sm:mb-0 w-2/3 primary-bg text-white"
                           href="{{ localeUrl('/valuation') }}">{{ trans('button.book_a_valuation') }}</a>
                    @endif

                    <a style="border-color: var(--ap-cta-bg);"
                       class="find-a-property-button whitespace-nowrap lg:text-sm text-base text-center font-bold rounded-3xl max-w-xs block sm:ml-4 py-3 px-8 transition-all w-2/3 border text-white"
                       href="{{ localeUrl('/all-properties-for-sale') }}">
                        {{ trans('button.find_a_property') }}
                    </a>
                </div>
            </div>
        </div>


    </div>

    <div class="col-span-2 xl:col-span-3 hidden lg:flex lg:justify-between items-center align-items-center flex-col lg:flex-row h-full secondary-bg">

        <div class="w-full h-full w-full overflow-hidden relative order-1 lg:order-2">
            <div class="absolute video-overlay w-full h-full inset bg-black z-10" @if($video->overlay_opacity) style="opacity: {{ $video->overlay_opacity }}"
                 @else style="opacity: 0;" @endif></div>

            @if($video->provider === 'internal')
                <video class="w-full h-full block transform scale-275 sm:scale-300 md:scale-250 lg:scale-400 xl:scale-300" style="filter: blur(20px); " muted autoplay>
                    <source src="{{ $video->getEmbedUrl() }}" type="video/mp4"/>
                    Your browser does not support the video tag.
                </video>
                <video class="w-full h-full block transform" muted autoplay>
                    <source src="{{ $video->getEmbedUrl() }}" type="video/mp4"/>
                    Your browser does not support the video tag.
                </video>
            @else
                <iframe loading="lazy" class="w-full h-full block transform scale-275 sm:scale-300 md:scale-250 lg:scale-400 xl:scale-300" src="{{ $video->getEmbedUrl() }}"
                        title="Video Player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            @endif
        </div>
    </div>


</div>
